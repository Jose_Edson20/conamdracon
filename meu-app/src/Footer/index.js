import React from 'react';
import './style.css'

import Icon from '../Img/facebook-icon.ico'
import Img from '../Img/instagram-icon.ico'

export default function Footer() {
    return (
        <footer>
          <div className = "background-green">

             <div align  = "center">
                <a href = "https://www.facebook.com/Conamdracon">

                    <i className = "icon-facebook"></i>
                      <img src = {Icon} alt = "Icone Facebook"/>
                </a>

                <a href = "https://www.instagram.com/conamdracon/">

                    <i className = "icon-instagram"></i>
                      <img src = {Img} alt = "Icone Instagram"/>
                      <p>Sigam nossas redes sociais </p>
                </a>

            </div>

          </div>

            <div className = "footer">

                  <a className = "s-12" href="App.js">
                     <p className = "corpy">Todos os direitos reservados &copy 2021 </p>
                  </a>

                <div className = "developer">
                    <a className = "text-size-12" href = "#"> 
                      <p>Desenvolvido por Jobson & José Edson</p>
                   </a>
               </div>
            </div>

        </footer>
    );
}

