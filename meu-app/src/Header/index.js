import React from 'react';
import './style.css'

function Header() {
    return (
     <header>  
       <div className = "header-container">
          <nav>
            <ul>
                <li><a href = "App.js">Início</a></li>
                <li><a href = "#">Edital</a></li>
                <li><a href = "#">Programação</a></li>
                <li><a href = "https://www.sympla.com.br/">Inscrições</a></li> 
                <li><a href = "#">Palestrantes</a></li>
            </ul>
          </nav>
     </div>
    </header>

    );
}

export default Header;
