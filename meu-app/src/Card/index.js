import React from 'react';
import './style.css'

import Img from '../Img/Uniesp.jpg';
import img from '../Img/FEBRARARAS_LOGO.png'
import Image from '../Img/LogoASPADOR.png'
import Logo from '../Img/Instituto_Palate.png'
import Foto from '../Img/foto-homem.jpg'
 
export default function Card() {
    return (
        <div className = "card-container">
            <h2>BEM-VINDO(A) A II EDIÇÃO DO EVENTO CONAMDRACON</h2>
            <p>
                O Evento
                Nos dias 03, 04 e 05 de novembro de 2021, a cidade de João Pessoa, no Estado da Paraíba, irá sediar um dos mais importantes 
                eventos de Doenças Raras e Anomalias Congênitas, realizado no Brasil. Um encontro que mobilizará profissionais, pesquisadores e estudantes de diversas áreas, oriundos de diversas regiões do Brasil e do Exterior. <br/> <br/>
                O evento contará com palestrantes de renome nacional e internacional, instituições de ensino da região, associações de classe, além de importantes empresas do setor. <br/>
  
                O objetivo desse evento é alavancar e fortalecer os pesquisadores e profissionais de alta performance no país, facilitando a acessibilidade de Profissionais e acadêmicos da saúde e áreas afins a um evento de elevado nível científico e de logística, compatível com os melhores congressos de especialidades realizados nos grandes centros do Brasil e do mundo. <br/> <br/>
                Além disso, tem o objetivo de aproximar pesquisadores, profissionais, estudantes, universidades, empresas, entre outras instituições ligadas ao estudo, a pesquisa e a assistência no âmbito das Doenças Raras e Anomalias Congênitas. 
                Tema Central
                O tema central do I CONAMDRACON será “Doenças raras e Anomalias Congênitas: desafios na busca pelo cuidado integral”. <br/> <br/>
                Baseado nessa temática, serão discutidos nesse evento, as novas condutas, além de protocolos clínicos e científicos, com o objetivo de usufruir dos novos recursos tecnológicos disponíveis nas diversas especialidades, na busca de tratamentos mais previsíveis a longo prazo.
            </p>

            <section className = "section">
                <div className = "line">
                    <h2>Convidados</h2>  
                    <p>
                        <img src = {Foto} alt = "foto-convidados" width = "150px"/>
                        Dr. William Henzel, médico renomado em neurocirurgia, pela Universidade
                        de Oxford USA.
                    </p>

                  <p className = "paragrafo">
                      <img src = {Foto} alt = "foto-convidados" width = "150px"/> 
                      Dr. Lukas Henry, médico renomado em doenças congênitas, formado
                      pela Universidade de Ohio Arizona.
                  </p>

                </div>
         </section>

        <section className = "background-blue">
            <a className = "local-evento"></a>

          <div className = "s-12">
             <p>
                  <img src = {Img} alt = "Uniesp" align = "left"/>
             </p>
          </div>

         <div className = "s-12">
             <div className = "padding">

                 <div className = "title">
                    <h3 className = "text-thin headline text-line">
                        Local do Evento
                    </h3>

                    <h4 className = "text-size text-height">
                        Centro Universitário (UNIESP)
                    </h4>
                    
                    <p className = "paragrafo">
                       Como confirmação de nosso compromisso essencial com a democratização do acesso ao Ensino Superior de qualidade, 
                       a unificação de toda a nossa rede de Faculdades e Centros Universitários sob a direção de uma única organização mantenedora,
                       <br/>
                       <br/>
                        projeta-se como marco no cenário da Educação no Brasil pelo que já fizemos até aqui e
                        pelo muito que nos dispomos a fazer pela formação profissional, humana, ética e cultural da juventude de nosso país.

                     <br/>
                     <br/>
                        Rodovia BR 230 Km 14 s/n Morada Nova, PB, 58109-3030
                    </p>

                 </div>

             </div>
         </div>

         <div className = "map map-center">
             <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3959.2714730755174!2d-34.85062328522607!3d-7.094499194876497!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7acdd0e422da873%3A0x252e780cac7ff41c!2sUNIESP%20Centro%20Universit%C3%A1rio!5e0!3m2!1spt-BR!2sbr!4v1618075775450!5m2!1spt-BR!2sbr" width="100%" height="380"></iframe>
         </div>

        </section>

        <section className = "Background-white">
            <div className = "div-line">
                <div className = "l-12 m-6 l-16 text-center">
                    <div align = "center">Realização</div>
                     <div align = "center">
                         <img src = {img} alt = "patriocionio"/>
                     </div>
                </div>

                <div className = "m-4 m-14">
                    <div align = "center">Patriocínio</div>
                     <div align = "center">
                         <img src = {Image} alt = "patriocinadores" width = "230px"/>
                     </div>

                    <div align = "center">
                        <img src = {Logo} alt = "Logo"/>
                    </div>

                </div>

            </div>
        </section>

        </div>

    );
}